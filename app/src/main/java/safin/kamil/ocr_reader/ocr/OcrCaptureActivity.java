/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package safin.kamil.ocr_reader.ocr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Location;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.text.TextRecognizer;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import safin.kamil.ocr_reader.PermissionUtil;
import safin.kamil.ocr_reader.R;
import safin.kamil.ocr_reader.SharedPref;
import safin.kamil.ocr_reader.model.Form;
import safin.kamil.ocr_reader.network.CachedCarsProvider;
import safin.kamil.ocr_reader.network.FormsProvider;
import safin.kamil.ocr_reader.network.LoadCarsProvider;
import safin.kamil.ocr_reader.ui.camera.CameraSource;
import safin.kamil.ocr_reader.ui.camera.CameraSourcePreview;
import safin.kamil.ocr_reader.ui.camera.GraphicOverlay;
import safin.kamil.ocr_reader.settings.SettingsActivity;

public final class OcrCaptureActivity extends AppCompatActivity {
    private static final String TAG = OcrCaptureActivity.class.getName();
    private static final String BLOCKED = TAG + "BLOCKED";
    private static final String PANEL_STATE = TAG + "PANEL_STATE";

    private static final int RC_HANDLE_GMS = 9001;

    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";

    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<OcrGraphic> mGraphicOverlay;

    private ScaleGestureDetector scaleGestureDetector;
    private TextToSpeech tts;

    @BindView(R.id.indicator_active)
    AVLoadingIndicatorView indicatorActive;
    @BindView(R.id.indicator_stopped)
    AVLoadingIndicatorView indicatorStopped;

    @BindView(R.id.number)
    EditText numberView;

    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;

    @BindView(R.id.send)
    Button send;

    @BindView(R.id.empty_value)
    View emptyValue;

    @BindView(R.id.error)
    TextView error;

    @BindView(R.id.sending_indicator)
    AVLoadingIndicatorView sendingIndicator;

    @BindView(R.id.car)
    Spinner car;

    @BindView(R.id.start)
    RadioButton startButton;

    @BindView(R.id.location_text)
    TextView locationText;

    private boolean blocked = false;
    private Location userLocation;
    private ArrayAdapter<String> spinnerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ocr_capture);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            blocked = savedInstanceState.getBoolean(BLOCKED);

            if (PermissionUtil.isLocationPermissionGranted(OcrCaptureActivity.this)) {
                handleLocation();
            }

            handleBlockScreen(!blocked);

            SlidingUpPanelLayout.PanelState panelState = (SlidingUpPanelLayout.PanelState) savedInstanceState.getSerializable(PANEL_STATE);
            slidingLayout.setPanelState(panelState);
            handlePanelState(panelState);

        } else {
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }

        slidingLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                handlePanelState(newState);

                if (PermissionUtil.isLocationPermissionGranted(OcrCaptureActivity.this)) {
                    handleLocation();
                }
                setLocationText();
            }
        });


        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1);

        sendingIndicator.hide();

        numberView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().isEmpty()) {
                    emptyValue.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.graphicOverlay);

        boolean autoFocus = true;
        boolean useFlash = false;

        if (PermissionUtil.isCameraPermissionGranted(this) &&
                PermissionUtil.isLocationPermissionGranted(this)) {
            createCameraSource(autoFocus, useFlash);
            handleLocation();
        } else {
            requestPermission();
        }

        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        TextToSpeech.OnInitListener listener =
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(final int status) {
                        if (status == TextToSpeech.SUCCESS) {
                            tts.setLanguage(Locale.US);
                        }
                    }
                };
        tts = new TextToSpeech(this.getApplicationContext(), listener);

        car.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPref.setLastSelected(OcrCaptureActivity.this, i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void handlePanelState(SlidingUpPanelLayout.PanelState state) {
        if (state != SlidingUpPanelLayout.PanelState.EXPANDED) {
            send.setBackground(ContextCompat.getDrawable(OcrCaptureActivity.this, R.drawable.rect_ready));
            send.setText(R.string.ready);

            error.setVisibility(View.GONE);
        } else {
            send.setBackground(ContextCompat.getDrawable(OcrCaptureActivity.this, R.drawable.rect_send));
            send.setText(R.string.send);

            error.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(BLOCKED, blocked);
        outState.putSerializable(PANEL_STATE, slidingLayout.getPanelState());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.user:
                SettingsActivity.start(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressLint("MissingPermission")
    private void handleLocation() {

        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        setLocationText();
                        if (location != null) {
                            userLocation = location;
                        }
                    }
                });
    }

    private void requestPermission() {
        String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        PermissionUtil.requestNotGrantedPermissions(this, permissions, PermissionUtil.GET_ACCESS_STORAGE_PERMISSION);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean b = scaleGestureDetector.onTouchEvent(e);

        return b || super.onTouchEvent(e);
    }

    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();

        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();
        OcrDetectorProcessor processor = new OcrDetectorProcessor(mGraphicOverlay);

        processor.setOnNumberFound(new OcrDetectorProcessor.OnNumberFound() {
            @Override
            public void onNumberFound(final String number) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!blocked) {
                            String formatted = new StringBuilder(number).insert(number.length() - 3, " ").toString();
                            numberView.setText(formatted);
                        }
                    }
                });
            }
        });

        textRecognizer.setProcessor(processor);

        if (!textRecognizer.isOperational()) {
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }
        mCameraSource =
                new CameraSource.Builder(getApplicationContext(), textRecognizer)
                        .setFacing(CameraSource.CAMERA_FACING_BACK)
                        .setRequestedPreviewSize(1024, 768)
                        .setRequestedFps(30.0f)
                        .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                        .setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null)
                        .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();

        new CachedCarsProvider(this).loadCars(new LoadCarsProvider.Callback() {
            @Override
            public void onSuccess(List<String> cars, boolean cached) {
                spinnerAdapter.clear();
                spinnerAdapter.addAll(cars);
                car.setAdapter(spinnerAdapter);

                int position = SharedPref.getLastSelected(OcrCaptureActivity.this);
                if (position < cars.size()) {
                    car.setSelection(position);
                }

                if (!cached) {
                    Snackbar.make(findViewById(android.R.id.content), R.string.cars_updated, Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(findViewById(android.R.id.content), R.string.loading_cars_error, Snackbar.LENGTH_LONG).show();
            }
        });

        final String userName = SharedPref.getUserName(this);
        final String userSurname = SharedPref.getUserSurname(this);

        if (userName != null && userSurname != null) {
            getSupportActionBar().setTitle(userName + " " + userSurname);
        } else {
            getSupportActionBar().setTitle(getString(R.string.no_user));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.RC_HANDLE_CAMERA_PERM) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, false);
                boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);
                createCameraSource(autoFocus, useFlash);
                return;
            }

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Multitracker sample")
                    .setMessage(R.string.no_camera_permission)
                    .setPositiveButton(R.string.ok, listener)
                    .show();

        } else if (requestCode == PermissionUtil.GET_ACCESS_STORAGE_PERMISSION) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, false);
                boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);
                createCameraSource(autoFocus, useFlash);
                handleLocation();
                return;
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startCameraSource() throws SecurityException {
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    @OnClick(R.id.mask)
    void onMaskClick() {
        handleBlockScreen(blocked);
        blocked = !blocked;
    }

    private void handleBlockScreen(boolean blocked) {
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(OcrCaptureActivity.this, R.anim.fade);
        if (blocked) {
            numberView.clearAnimation();
            numberView.setTextColor(ContextCompat.getColor(OcrCaptureActivity.this, R.color.colorPrimary));
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        } else {
            numberView.startAnimation(myFadeInAnimation);
            numberView.setTextColor(ContextCompat.getColor(OcrCaptureActivity.this, R.color.colorAccent));
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }

        indicatorActive.setVisibility(blocked ? View.VISIBLE : View.GONE);
        indicatorStopped.setVisibility(blocked ? View.GONE : View.VISIBLE);
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            if (mCameraSource != null) {
                mCameraSource.doZoom(detector.getScaleFactor());
            }
        }
    }

    @OnClick(R.id.send)
    void onSendClick() {
        error.setVisibility(View.GONE);
        send.setText(R.string.send);

        if (slidingLayout.getPanelState() != SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        } else {

            final String name = SharedPref.getUserName(this);
            final String surname = SharedPref.getUserSurname(this);


            String start = startButton.isChecked() ? "start" : "stop";

            boolean valid = true;

            if (name == null && surname == null) {
                valid = false;
                error.setVisibility(View.VISIBLE);
                error.setText(R.string.no_user_selected);
            }

            if (numberView.getText().toString().isEmpty()) {
                emptyValue.setVisibility(View.VISIBLE);
                valid = false;
            } else {
                emptyValue.setVisibility(View.GONE);
            }

            if (!valid) {
                return;
            }

            String location;
            if (userLocation == null) {
                location = getString(R.string.none);
            } else {
                location = userLocation.getLatitude() + ";" + userLocation.getLongitude();
            }

            sendingIndicator.show();
            String number = numberView.getText().toString().replace(" ", "");

            Form form = new Form(name, surname, number, start, location, spinnerAdapter.getItem(car.getSelectedItemPosition()));
            new FormsProvider().sendForm(form, new FormsProvider.Callback() {
                @Override
                public void onSuccess() {
                    sendingIndicator.hide();
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                    Snackbar.make(findViewById(android.R.id.content), R.string.success, Snackbar.LENGTH_LONG).show();
                }

                @Override
                public void onFailure() {
                    sendingIndicator.hide();
                    error.setText(R.string.error);
                    error.setVisibility(View.VISIBLE);
                    send.setText(R.string.send_again);
                }
            });
        }
    }

    private void setLocationText() {
        if (userLocation == null) {
            locationText.setText(R.string.locaiton_empty);
            locationText.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        } else {
            locationText.setText("szer: " + String.format("%.3f", userLocation.getLatitude())
                    + " dł: " + String.format("%.3f", userLocation.getLongitude()));
            locationText.setTextColor(ContextCompat.getColor(this, android.R.color.black));
        }
    }
}
