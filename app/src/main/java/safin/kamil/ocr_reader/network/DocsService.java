package safin.kamil.ocr_reader.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DocsService {

    @POST("1FAIpQLSfPKNkNYbtiST4VibkWJDM9QMC9dPC_wRlY8-A7UXCuKPyU1Q/formResponse")
    @FormUrlEncoded
    Call<Void> sendToDocs(
            @Field("entry.1491758966") String name,
            @Field("entry.1750699455") String surname,
            @Field("entry.846445897") String meter_status,
            @Field("entry.692264878") String start_stop,
            @Field("entry.1566846757") String location,
            @Field("entry.88786082") String car
    );

}
