package safin.kamil.ocr_reader.database;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

import java.util.UUID;

@Table(database = AppDatabase.class)
public class Car {

    @PrimaryKey
    UUID id;
    @Column
    String name;

    public Car() {
    }

    public Car(String name) {
        this.id = UUID.randomUUID();
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
