package safin.kamil.ocr_reader.network;

import java.util.List;

public interface LoadCarsProvider {

    void loadCars(final Callback callback);

    interface Callback {
        void onSuccess(List<String> cars, boolean cached);

        void onFailure(Throwable t);
    }
}
