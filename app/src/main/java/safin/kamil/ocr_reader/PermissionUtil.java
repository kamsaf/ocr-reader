package safin.kamil.ocr_reader;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;

public class PermissionUtil {

    public static final int RC_HANDLE_CAMERA_PERM = 2;
    public static final int GET_ACCESS_STORAGE_PERMISSION = 3;

    public static boolean isLocationPermissionGranted(Context context) {
        return ((ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED));
    }

    public static boolean isCameraPermissionGranted(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestNotGrantedPermissions(Activity activity, String[] permissions, int requestCode) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_DENIED) {
                permissionsToRequest.add(permission);
            }
        }

        if (permissionsToRequest.size() > 0) {
            String[] request = new String[permissionsToRequest.size()];
            ActivityCompat.requestPermissions(activity, permissionsToRequest.toArray(request), requestCode);
        }
    }
}
