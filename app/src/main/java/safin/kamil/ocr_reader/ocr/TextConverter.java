package safin.kamil.ocr_reader.ocr;

class TextConverter {

    String convert(String text) {

        if (text.length() < 5) {
            return null;
        }

        String converted = "";

        String numeric = text.replace("S", "5").replace("s", "5");
        numeric = numeric.replace("I", "1").replace("i", "1");
        numeric = numeric.replace("E", "6");
        numeric = numeric.replace("B", "6");
        numeric = numeric.replace("D", "0");
        numeric = numeric.replace("T", "7");
        numeric = numeric.replace("l", "1");
        numeric = numeric.replace(".", " ");

        String[] test = numeric.split("\\n");

        boolean hasCounter = false;
        for (String s : test) {
            s = s.replaceAll("[^\\d.]", "");
            if (s.length() == 5) {
                converted = s.substring(0, 5);
                hasCounter = true;
            } else if (s.length() > 5) {
                converted = s.substring(0, 6);
                hasCounter = true;
            }
        }

        if (!hasCounter) {
            return null;
        }

        return converted;
    }
}
