package safin.kamil.ocr_reader.base;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import safin.kamil.ocr_reader.base.BaseItemHolder;

public abstract class BaseListAdapter<T> extends RecyclerView.Adapter<BaseItemHolder<T>>{

    private BaseItemHolder.OnItemClickListener<T> onItemClickListener;
    protected List<T> items = new ArrayList<>();

    public BaseListAdapter(BaseItemHolder.OnItemClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setData(List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    public abstract BaseItemHolder<T> createBaseItemHolder(ViewGroup parent, int viewType);

    @Override
    public BaseItemHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseItemHolder<T> baseItemHolder = createBaseItemHolder(parent, viewType);
        baseItemHolder.setOnItemClickListener(onItemClickListener);
        return baseItemHolder;
    }

    @Override
    public void onBindViewHolder(BaseItemHolder<T> holder, int position) {
        holder.bindItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}