package safin.kamil.ocr_reader.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseItemHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected T item;

    public OnItemClickListener<T> onItemClickListener;

    public BaseItemHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        setClickListener(itemView);
    }

    protected void setClickListener(View itemView) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(item);
                }
            }
        });
    }

    protected void setOnItemClickListener(OnItemClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void bindItem(T item) {
        this.item = item;
        onItemBind(item);
    }

    protected abstract void onItemBind(T item);

    @Override
    public void onClick(View v) {
        onItemClickListener.onItemClick(item);
    }

    public interface OnItemClickListener<T> {
        void onItemClick(T item);
    }
}